Inclusion de archivos include,include\_once,require,require\_once
------------------------------------------------------------------

En PHP es necesario especificar explícitamente los archivos que serán utilizados durante la ejecución de una petición. Para ello se utilizan tres construcciones del lenguaje (ojo **no** son funciones): include, include\_once, require y require\_once.

La principal diferencia entre las construcciones include y las require radica en que, las construcciones include mandarán una advertencia (mensaje de nivel de error Warning) cuando no se encuentre el archivo especificado, lo cual no detendrá la ejecución del script; mientras que las construcciones require arrojan un error fatal del cual php no puede recuperarse y terminan inmediatamente la ejecución del script.

La diferencia que existe entre las construcciones include e include\_once (que también aplica para require y require once) es que, como su nombre indica, include\_once revisa que un archivo no haya sido incluido para incluirlo, y si ya ha sido incluido entonces include\_once regresará el valor booleano True y no volverá a incluir el archivo. La utilidad de las funciones que terminan en \_once radica en que evitan la redefinición de funciones y asignaciones a variables.

Cómo se realiza la búsqueda de archivos
---------------------------------------

La búsqueda de archivos a incluir se hace en base al archivo que quiere
hacer la inclusión, o si ninguna es ingresada, comenzará a buscar en el
include\_path declarado en la configuración de PHP (php.ini). Si la ruta
es absoluta o relativa al directorio actual (si la ruta comienza con . o
..) se ignora el include\_path. Si el archivo a incluir no se encuentra
en el include\_path, php buscará en el directorio del script que quiere
hacer la inclusión. Si aun así no encuentra el archivo, PHP buscará
finalmente en el directorio de trabajo

Contexto de inclusión
---------------------

Una vez incluido el archivo, este hereda el ámbito de las variables de
la línea en la cual ocurrió la inclusión. Las variables estarán
disponibles de ese punto en adelante, aunque si el archivo incluido
define clases o funciones, estas estarán disponibles a nivel global.

Notas adicionales
-----------------

Cuando se incluye un archivo, PHP abandona el modo de interpretación de
código PHP y entra en su modo de parseo HTML, por eso es necesario que
se usen los tags &lt;?php y ?&gt; para hacer que el intérprete vuelva al
modo de PHP

Una práctica común es utilizar los archivos incluidos para generar
vistas compuestas.

Es una buena práctica utilizar siempre require\_once, a menos que se
trabaje en aplicaciones pequeñas o que se utilice un autoloader.

La ruta de inclusión se hace en base a la ruta del primer archivo que se
ejecutó, lo que hace que los archivos que incluyan otros archivos hagan
dicha inclusión en base a la ruta respecto al primer archivo

Alcance/Ámbito (scope) de variables en PHP
==========================================

En PHP existen tres niveles de alcance: global, local y estático. Cuando
una variable no se incluye dentro del contexto de una función, se dice
que es de alcance global. Aquellas variables que se encuentren definidas
dentro del contexto de una función pueden ser locales o estáticas. Una
variable estática definida dentro de una función retiene su valor una
vez que ha finalizado la ejecución de dicha función. Una variable local
no tiene este comportamiento, es decir, pierde su valor una vez que se
ha finalizado la ejecución de la función.

Para definir una variable global, simplemente hay que definirla como
cualquier variable, excepto que fuera de cualquier contexto.

Para utilizar una variable global dentro de un contexto local se
utilizan dos mecanismos, utilizando la palabra reservada **global** o
accediendo al array \$GLOBALS.

Para definir una variable como estática, se utiliza la palabra reservada
**static**. El valor asignable a una variable estática no puede ser el
resultado de una expresión.

Notas adicionales
-----------------

Las variables definidas dentro de un objeto se consideran propiedades
del mismo. Si no tienen definido un modificador de acceso, por defecto
este es público.

A diferencia de javascript, al utilizar closures en PHP, las variables
no se heredan dentro del contexto del closure implícitamente, para poder
acceder a ellas es necesario hacer uso de la palabra reservada use

Conversión de tipos
===================

PHP es un lenguaje de tipado dinámico que no permite la especificación
explicita de tipos (a diferencia de lenguajes como Groovy), el tipo de
la variable es determinado por el contexto en que se utiliza la misma.

Al utilizar los diferentes operadores que tiene PHP (que se verán más
adelante) sobre una o más variables, PHP realizará una evaluación de la
expresión para convertir a los operandos en el tipo pertinente y
regresará un valor con el mismo tipo.

Algunos ejemplos:

\$foo = "0"; // \$foo es string (ASCII 48)

\$foo += 2; // \$foo es ahora un integer (2)

\$foo = \$foo + 1.3; // \$foo es ahora un float (3.3)

\$foo = 5 + "10 Cerditos pequeñitos"; // \$foo es integer (15)

\$foo = 5 + "10 Cerdos pequeños"; // \$foo es integer (15)

Además de la conversión automática, PHP soporta el forzado de tipos, lo
que indica al intérprete de PHP que debe interpretar una variable como
de un cierto tipo

Para forzar un tipo solo es necesario anteponer a la variable el tipo
encerrado entre paréntesis.

Los siguientes forzados de tipo están permitidos:

-   (int), (integer) - forzado a integer

-   (bool), (boolean) - forzado a boolean

-   (float), (double), (real) - forzado a float

-   (string) - forzado a string

-   (array) - forzado a array

-   (object) - forzado a object

-   (unset) - forzado a NULL

Operadores de PHP
=================

Los siguientes son los operadores soportados en PHP. Dependiendo del
tipo de operador, se hace el casteo automático de los operandos (como se
vio en la sección anterior)

  Asociatividad   Operadores                                                  Información adicional
  --------------- ----------------------------------------------------------- -------------------------------------------------
  no asociativo   clone new                                                   clone and new (clonación y creación de objetos)
  izquierda       \[                                                          
  derecha         \*\*                                                        aritmética
  derecha         ++ -- \~ (int) (float) (string) (array) (object) (bool) @   tipos e incremento/decremento y stfu
  no asociativo   instanceof                                                  tipos
  derecha         !                                                           lógico
  izquierda       \* / %                                                      aritmética
  izquierda       + - .                                                       aritmética y string
  izquierda       &lt;&lt; &gt;&gt;                                           bit a bit
  no asociativo   &lt; &lt;= &gt; &gt;=                                       comparación
  no asociativo   == != === !== &lt;&gt; &lt;=&gt;                            comparación
  izquierda       &                                                           bit a bit y referencias
  izquierda       \^                                                          bit a bit
  izquierda       |                                                           bit a bit
  izquierda       &&                                                          lógico
  izquierda       ||                                                          lógico
  derecha         ??                                                          comparación
  izquierda       ? :                                                         ternario
  derecha         = += -= \*= \*\*= /= .= %= &= |= \^= &lt;&lt;= &gt;&gt;=    asignación
  izquierda       and                                                         lógico
  izquierda       xor                                                         lógico
  izquierda       or                                                          lógico

Notas adicionales
-----------------

PHP no soporta la sobre-escritura de operadores (con excepción quizás de
\[\]).

Funciones
=========

Las funciones en los lenguajes de programación imperativa y funcionales,
permiten encapsular/modularizar comportamientos para poder
re-utilizarlos durante la ejecución de un programa. Para definir una
función en PHP se utiliza la palabra reservada **function** seguida del
nombre de la función (opcional) y dentro de paréntesis se especifican
los parámetros de la función**.**

Para llamar una función se pueden utilizar los siguientes métodos:

-   Utilizando el nombre de la función seguida del operador “()” con los
    argumentos que recibe la función

-   Utilizando una variable cuyo valor sea un string con el nombre de la
    función, seguida del operador “()” con los argumentos a enviar
    (funciones variables)

-   Utilizando las funciones call\_user\_func y call\_user\_func\_array

Las funciones de retrollamada o callbacks, se utilizan para pasar a una
función otra función como argumento. Por ejemplo, en javascript es muy
común utilizar callbacks para manejar eventos. PHP también tiene el
concepto de callbacks.

Los callables son una forma de type hinting utilizada a partir de PHP
5.4, esto ayuda a especificar que una función va a recibir como
parámetro otra función.

Type hinting es la capacidad de especificar en una función el tipo de
parámetros que va a recibir. Antes de PHP 7 solo se podía hacer type
hinting de objetos, de arrays y de callables.

Las funciones anónimas, simplemente son funciones que no tienen un
nombre. Es muy común utilizarlas como parámetros hacia una función que
espera un callable. En PHP es común que se utilice el concepto de
función anónima y el de closure como sinónimos.

Notas adicionales
-----------------

A diferencia de javascript, en PHP no se hereda el contexto de las
variables en las que se defina un closure. Si se necesita acceder a
variables del contexto se puede utilizar la palabra reservada **using**

Autoloaders
===========

A diferencia de lenguajes como C\# o Java, en lenguajes como C, C++ o
PHP es necesario especificar desde que archivos se van a cargar las
clases que se van a utilizar durante la ejecución (compilación en caso
de C, C++) del programa.

En PHP se tienen las funciones include y require, sin embargo, conforme
va creciendo una aplicación, puede llegar a ser muy complicado el hacer
un seguimiento de que archivos se requiere incluir para poder hacer una
correcta resolución de clases y espacios de nombres. Por ello PHP
permite definir una pila de funciones a llamar durante la resolución de
nombres de clases y espacios de nombres.

PHP cuenta con funciones para especificar las estrategias de auto-carga
de clases:

-   \_\_autoload

-   spl\_autoload\_register

-   spl\_autoload\_unregister

Composer
--------

Conforme aumentan el tamaño y requerimientos de una aplicación, puede
ser necesario hacer uso de bibliotecas externas que a su vez pueden
tener dependencias de otras bibliotecas. En java podemos encontrar
soluciones como maven o gradle, para .Net se encuentra nugget, para
nodejs se utiliza npm. En el caso de PHP lo más común es utilizar la
herramienta Composer.

Composer no solo permite fácilmente la descarga de dependencias, sino
que permite además especificar las versiones a utilizar de dichas
dependencias, generar un autoloader optimizado que permita llamar a las
dependencias sin preocupaciones y utilizar ese mismo autoloader para
hacer la carga de las clases propias de la aplicación y otras varias
cosas.

Para instalar composer, simplemente hay que descargar el phar de la
página de composer
([*https://getcomposer.org/download/*](https://getcomposer.org/download/))
y agregarlo al path del sistema para poder utilizarlo desde cualquier
directorio del sistema.

Una vez instalado es posible comenzar a utilizar composer creando un
archivo llamado composer.json dentro del directorio raíz del proyecto.
En dicho archivo se especifican las características del proyecto, las
dependencias, las rutas de las clases/espacios de nombres a cargar, etc.

Algunos comandos útiles de composer son los siguientes:

-   init: Permite generar el archivo composer.json de manera interactiva

-   install: Permite instalar las dependencias especificadas en
    el composer.json. Además genera un archivo llamado composer.lock
    cuyo contenido principal son las versiones exactas descargadas
    por composer.

-   update: Actualiza las dependencias de acuerdo al composer.json y
    actualiza el composer.json

-   require: Permite instalar una nueva dependencia

-   remove: Permite eliminar algún paquete instalado por composer

-   global: Permite el manejo de dependencias a nivel global. Útil para
    instalar plugins de composer o herramientas de CLI

-   self-update: Actualiza composer

-   dump-autoload: Actualiza el autoloader generado por composer. Útil
    si se especifican clases propias para ser cargadas por el autoloader
    en el composer.json

Ejemplo sencillo de archivo composer.json

{

"name": "pablo/curso-php",

"description": "Curso de php intermedio-avanzado",

"require": {

"monolog/monolog": "\^1.19"

},

"authors": \[

{

"name": "Pablo Flores",

"email": "pablo.flores@netlogistik.com"

}

\],

"autoload": {

"psr-4": {

"un\\\\": "unFolder/"

}

}

Notas adicionales
-----------------

Se recomienda no versionar la carpeta vendor donde se encuentran las
dependencias descargadas de composer. No así con los archivos
composer.json y composer.lock

Expresiones regulares en PHP
============================

Una de las herramientas más potentes para análisis de textos son las
expresiones regulares. Las expresiones regulares permiten el
reconocimiento de todos y únicamente los lenguajes regulares (aquellos
lenguajes que pueden ser reconocidos por un autómata finito
determinista). Adicionalmente, algunos motores de reconocimiento de
expresiones regulares soportan funcionalidades que permiten a las
expresiones reconocer lenguajes que no son regulares.

PHP tiene una biblioteca para el reconocimiento de expresiones regulares
PCRE (Perl Compatible Regular Expressions). En dicha biblioteca se
pueden encontrar algunas funciones útiles para la inspección y reemplazo
de cadenas utilizando expresiones regulares.

Sintaxis básica
---------------

### Delimitadores

Una expresión regular debe tener un par de delimitadores. Dichos
delimitadores pueden ser cualquier carácter no alfanumérico, que no sea
una barra invertida (\\) o espacio en blanco. Se recomienda utilizar el
carácter / para delimitar las expresiones regulares.

Algunos ejemplos:

/esta\\ses\\suna\\sexpresion\\sregular/

\#esta\\ses\\suna\\sexpresion\\sregular\#

### Opciones adicionales

Es posible especificar opciones adicionales después del delimitador
final, como por ejemplo “i” para volver la expresión regular insensible
a mayúsculas y minúsculas, u para hacer que la cadena pueda reconocer
caracteres UTF-8, etc.

Ejemplos:

/esta\\ses\\suna\\sexpresion\\sregular/iumx

/esta\\ses\\suna\\sexpresion\\sregular/i

### Metacaracteres 

Se dispone de caracteres especiales que no se interpretan literalmente y
que son utilizados para dar funcionalidad a las expresiones regulares.
La siguiente tabla muestra estos caracteres:

  \\   carácter de escape general
  ---- ------------------------------------------------------------------------------------------------------------------
  \^   declaración de inicio de sujeto. Si la cadena que se prueba no contiene la expresión al inicio, no se reconoce
  \$   declaración de fin de sujeto. Si la cadena que se prueba no contiene la expresión al final, no se reconoce
  .    coincide con cualquier carácter excepto con el de nueva línea
  \[   inicio de la definición de la clase carácter. Junto con \] permite definir rangos
  \]   fin de la definición de la clase carácter. Junto con \[ permite definir rangos
  |    inicio de rama alternativa. Permite especificar varias alternativas para un patrón
  (    inicio de sub-patrón
  )    fin de sub-patrón
  ?    amplia el significado de (, también cuantificador 0 o 1, también hace perezosos a los cuantificadores codiciosos
  \*   cuantificador 0 o más
  +    cuantificador 1 o más
  {    inicio de cuantificador mín/máx.
  }    fin de cuantificador mín/máx

Algunos ejemplos:

/\[a-z\]/i reconoce cualquier carácter alfabético (ASCII)

/\^\[a-z\]/i la cadena debe iniciar con un carácter alfabético (ASCII)

/\[a-z\]\$/i la cadena debe terminar con un carácter alfabético (ASCII)

/\^\[a-z\]{3,}/i La cadena debe contener al inicio al menos tres
caracteres alfabéticos

/\^\[a-z\]{3,4}/i La cadena debe contener al inicio al menos tres
caracteres alfabéticos y máximo debe tener 4

/\^\\.\*\$/ La cadena debe estar conformada únicamente por 0 o más
caracteres . (punto)

/\^.+\$/ La cadena debe estar formada únicamente por uno o más
caracteres (incluidos espacios, caracteres alfanuméricos y especiales)

/\^(perro|gato)\$/i La cadena debe iniciar con perro o terminar con gato